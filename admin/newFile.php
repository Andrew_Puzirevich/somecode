<?php

class Procedure
{
    public function DBConnect()
    {
        $host = 'bald00.mysql.tools';
        $db   = 'bald00_red';
        $user = 'bald00_red';
        $pass = '3Yeh!C7_9e';
        $charset = 'utf8';

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $pdo = new PDO($dsn, $user, $pass, $opt);
        return $pdo;

    }

    public function setProject($logo, $name, $date, $shAbout, $flAbout, $site, $adr, $phone, $color)
    {
        $pdo = $this->DBConnect();
        $date = strtotime($date);

        if ($name != '' && $pdo->exec("INSERT INTO `Projects`(`project_logo`, `project_name`, `create_date`, `short_about_pr`, `about_pr`, `site_url`, `adress_pr`, `phone_pr`, `logo_color`) VALUES ('$logo', '$name', '$date','$shAbout', '$flAbout', '$site','$adr', '$phone','$color')")){
            echo 'success';
        }
        else echo 'error';
    }

    public function updateProject($name, $date, $sh_about, $fl_about, $site, $address, $phone, $id)
    {
        $pdo = $this->DBConnect();
        $res = $pdo->query('UPDATE `Projects` SET project_name="'.$name.'", create_date="'.$date.'", short_about_pr="'.$sh_about.'", about_pr="'.$fl_about.'", site_url="'.$site.'", adress_pr="'.$address.'", phone_pr="'.$phone.'" WHERE id='.$id);
    }

    public function delProject($id)
    {
        $pdo = $this->DBConnect();
        $res = $pdo->query('DELETE FROM Projects where id='.$id);
    }

    public function getProjects()
    {
        $pdo = $this->DBConnect();

            $res = $pdo->query('SELECT * FROM Projects');


         return $res;
    }

    public function getProjectByID($id)
    {
        $pdo = $this->DBConnect();
        $res = $pdo->query('SELECT * FROM Projects WHERE id ='.$id);

        return $res;
    }

    public function setCase($name, $page, $date, $project/*, $content*/, $client, $preview, $fonColor, $fooColor, $seo_t, $seo_d, $firstFone)
    {
        $pdo = $this->DBConnect();

        if ($pdo->exec("INSERT INTO `Cases`(`case_page`, `date_publish`, `case_project`, `case_name`/*,  `case_content`*/, `project_prewiev`, `case_for_client`, `color_fon`, `color_head_foo`, `seo_title`, `seo_descr`, `first_fon_photo`) VALUES ('$page','$date','$project','$name','$preview','$client','$fonColor', '$fooColor', '$seo_t','$seo_d', '$firstFone')")){
            return true;
        } else return false;
    }

    public function getCases()
    {
        $pdo = $this->DBConnect();
        $res = $pdo->query('SELECT * FROM Cases');
        return $res;
    }

    public function delCase($id)
    {
        $pdo = $this->DBConnect();
        $pdo->query("DELETE FROM `Cases` WHERE id=".$id);
    }

    public function getCompany(){
        $pdo = $this->DBConnect();
        $res = $pdo->query('SELECT id,project_name FROM Projects');
        return $res;
    }

    public function getCaseByName($name)
    {
        $pdo = $this->DBConnect();
        $res = $pdo->query("SELECT * FROM Cases WHERE `case_page`='$name'");
        return $res;
    }

    public function getCaseById($id)
    {
        $pdo = $this->DBConnect();
        $res = $pdo->query("SELECT * FROM Cases WHERE id=".$id);
        return $res;
    }

    public function setNews($news, $date)
    {
        $pdo = $this->DBConnect();
        $pdo->query("INSERT INTO `News` (`news`, `news_date`) VALUES ('$news', '$date')");
    }

    public function getNews()
    {
        $pdo = $this->DBConnect();
        $res = $pdo->query("SELECT * FROM `News`");
        return $res;
    }
}