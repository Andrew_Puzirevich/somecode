<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "xhtml11.dtd">
<html>
<head>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1251" />
    <TITLE>Добы день!</TITLE>
    <link rel="stylesheet" href="style.css">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
</head>

<?php
require_once 'newFile.php';
$procredure = new Procedure();
//var_dump($_POST); exit();
if (isset($_POST['addCase'])){

    $firstBacground = '';
    if (isset($_FILES['first_fon'])){
        $uploaddir = $_SERVER['DOCUMENT_ROOT'].'uploadsCases'.DIRECTORY_SEPARATOR;
        $uploadfile = $uploaddir . basename($_FILES['first_fon']['name']);
        move_uploaded_file($_FILES['first_fon']['tmp_name'], $uploadfile);
        $firstBacground = DIRECTORY_SEPARATOR .'uploadsCases'.DIRECTORY_SEPARATOR . basename($_FILES['first_fon']['name']);
    }
if (isset($_FILES['caseFile'])){
$uploaddir = $_SERVER['DOCUMENT_ROOT'].'uploadsCases'.DIRECTORY_SEPARATOR;
$uploadfile = $uploaddir . basename($_FILES['caseFile']['name']);
if (move_uploaded_file($_FILES['caseFile']['tmp_name'], $uploadfile)) {
    $caseFileUrl = DIRECTORY_SEPARATOR .'uploadsCases'.DIRECTORY_SEPARATOR . basename($_FILES['caseFile']['name']);
}}

    $myfile = fopen("cases-data/".$_POST['casePage'].".txt", "w") or die("Unable to open file!");

    $content = str_replace('video controls=&quot;&quot;', 'video autoplay loop muted',$_POST['page_text']);
    $content = str_replace('Museo Sans Cyr 300', 'Open Sans', $content);
    $content = str_replace('<span','<div class="textCentrPortf"> <span', $content);
    $content = str_replace('</span>','</span></div>', $content);

    fwrite($myfile, htmlspecialchars($content));
//    var_dump(fclose($myfile)); exit();
    fclose($myfile);
    $procredure->setCase($_POST['caseName'],$_POST['casePage'],$_POST['caseDate'],$_POST['caseProject']/*, $_POST['caseContent']*/, $_POST['caseClient'], $caseFileUrl, $_POST['fon_color'], $_POST['foo_color'], $_POST['seo_title'], $_POST['seo_description'], $firstBacground);
}

if (isset($_POST['delete'])){
    $procredure->delCase($_POST['delete']);
}

//if (isset($_FILES['caseFile'])){
//    $uploaddir = $_SERVER['DOCUMENT_ROOT'].'uploadsCases'.DIRECTORY_SEPARATOR;
//    $uploadfile = $uploaddir . basename($_FILES['caseFile']['name']);
//    if (move_uploaded_file($_FILES['caseFile']['tmp_name'], $uploadfile)) {
//        $caseFileUrl = DIRECTORY_SEPARATOR .'uploadsCases'.DIRECTORY_SEPARATOR . basename($_FILES['caseFile']['name']);
//
//        ?><!--<script>-->
<!--            $("#img-loaded-case").load("/admin/cases #img-loaded-case > *");-->
<!--            $("#url-case-img").load("/admin/cases #url-case-img > *");-->
<!--            $(".block-block").css("display", "none");-->
<!--            $("#cases-block").css("display", "flex");-->
<!---->
<!---->
<!--        </script>--><?php
//
//    } else {
//
//        echo '<script>alert("Возможная атака с помощью файловой загрузки!")</script>';
//    }
//}
?>

<body>
<div>
    <div class="row col-md-12">
        <div class="col-2">
            <?php require_once 'menu.html';?>
        </div>
        <div class="col-md-10 row block-block" id="cases-block">
            <div class="tab-pane-inside">
                <h3>Добавить кейс</h3>
<!--                <div class="custom-file">-->
<!--                    <form enctype="multipart/form-data" method="POST">-->
<!--                        Отправить этот файл: <p></p>-->
<!--                        <button type="submit" name="submit">Загрузить</button>-->
<!--                    </form>-->
<!--                </div>-->

<!--                <div id="img-loaded-case">-->
<!--                    <img src="--><?php //echo $caseFileUrl; ?><!--" alt="" width="100">-->
<!--                </div>-->

                <form class="add-case" action="" enctype="multipart/form-data" method="POST">
                    <div class="form-group ">
                        <label for="projectLogo">Превью проекта</label>
                        <input name="caseFile" type="file" />

                    </div>
                    <div class="form-group">
                        <label for="caseName">Название кейса</label>
                        <input type="text" id="caseName" name="caseName">
                    </div>
                    <div class="form-group">
                        <label for="casePage" id="">Название кейса для URL (англ.)</label>
                        <input type="text" name="casePage" id="casePage">
                    </div>
                    <div class="form-group">
                        <label for="caseDate" id="">Дата публикации</label>
                        <input type="date" name="caseDate" id="caseDate">
                    </div>
                    <div class="form-group">
                        <label for="" id="">Услуга к которой относится</label>
                        <input type="text" name="caseService" id="">
                    </div>
                    <div class="form-group">
                        <label for="" id="">Клиент для которого разрабатывался</label>
                        <input type="text" name="caseClient" id="">
                    </div>
                    <div class="form-group">
                        <label for="caseProject" id="">Компания (Проект) которым разрабатывался</label>
                        <select name="caseProject" id="caseProject">
                        <?php
                        $companies = $procredure->getCompany();
                        while ($row = $companies->fetch(PDO::FETCH_ASSOC)){
                            ?>
                            <option value="<?php echo $row['id']?>"><?php echo $row['project_name']?></option>
                            <?php
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" id="">Ачивки, которые получал</label>
                        <input type="text" name="" id="">
                    </div>
                    <div class="form-group">
                        <label for="" id="">Люди, которые работали над проектом</label>
                        <input type="text" name="" id="">
                    </div>

<!--                    <div class="form-group">-->
<!--                        <label for="caseContent">Содержание проекта</label>-->
<!--                        <textarea name="caseContent" id="caseContent" rows="5"></textarea>-->
<!--                    </div>-->

                    <div class="form-group">
                        <label for="" id="">Отрасль к которой относится</label>
                        <input type="text" name="" id="">
                    </div>

                    <div class="form-group">
                        <div style="display: flex;">
                            <div style="width: 200px; height: 20px; color: #000; background-color: white">255,255,255 - белый</div>
                            <div style="background-color: black; color: white; width: 200px; height: 20px">0,0,0 - черный</div>
                        </div>
                        <label for="">Цвет фона кейса в rgb()</label>
                        <input placeholder="000,000,000" type="text" value="" name="fon_color">
                        
                        <label for="">Картинка фона кейса (первый экран)</label>
                        <input type="file" name="first_fon">
                    </div>
                    <div class="form-group">
                        <label for="">Цвет футера, хедера и текста в rgb()</label>
                        <input placeholder="000,000,000" type="text" value="" name="foo_color" >
                    </div>
                    <h5 style="font-weight: 700">Редактор страницы</h5>
                    <div class="form-group">
<!--                        <label for="pageSrc">Картинки</label>-->
<!--                        <input type="file" id="pageSrc" name="pageSrc[]" multiple >-->
                        <div id="summernote">

                        </div>
                        <script>
                            $(document).ready(function() {
                                $('#summernote').summernote();
                            });

                            $('#summernote').summernote({
                                callbacks: {
                                    onChange: function(contents, $editable) {
                                        console.log('onChange:', contents, $editable);
                                        $('#page_text').val(contents);
                                        // $('.res').html(contents);
                                        // $('#page_text').text(contents);

                                    }
                                }
                            });
                            // $('#ww').on('click', function () {
                            //     var content = $('.res').html();
                            //     alert(content);
                            // });
                        </script>
                    </div>

<!--                    <div class="res" style="display: none"></div>-->
<!--                    <div class="form-group">-->
<!--                        <label for="">Код страницы</label>-->
                        <input type="hidden" id="page_text" name="page_text" value="">
<!--                        <textarea name="page_text" id="page_text" cols="30" rows="10"></textarea>-->
<!--                    </div>-->
                    <h5>SEO</h5>
                    <div class="form-group">
                        <label for="" >Title кейса</label>
                        <input type="text" name="seo_title">
                    </div>
                    <div class="form-group">
                        <label for="">Description кейса</label>
                        <input type="text" name="seo_description">
                    </div>

<!--                    <div id="url-case-img">-->
<!--                        <input type="hidden" name="casePreviewUrl" id="casePreviewUrl" value="--><?php //echo $caseFileUrl; ?><!--">-->
<!--                    </div>-->
                    <div class="form-group">
                        <button class="btn btn-success" name="addCase" id="addCase">Добавить</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane-inside" id="cases-table">
                <h3>Список кейсов</h3>
                <table class="table table-striped">
                    <tbody>
                    <?php
                    $cases = $procredure->getCases();
                    if (!empty($cases)):
                        $iter = 1;
                        foreach ($cases as $case) {
                            ?>
                            <tr>
                                <th scope="row"><?php echo $iter; ?></th>
                                <td><img src="..<?php echo $case['project_prewiev']?>" alt="" width="100"></td>
                                <td><?php echo $case['case_name']?></td>
                                <td>
                                    <form action="https://www.bohdan.red/admin/edit" method="post">
                                        <input type="hidden" name="back" value="<?php echo $_SERVER['REQUEST_URI'];?>">

                                        <input type="hidden" name="idCase" value="<?php echo $case['id']?>">
                                        <button class="btn btn-info full-btn" name="editCase">Edit</button>

                                    </form>
                                    <button class="btn btn-danger delCase full-btn" id="delCase-<?php echo $case['id']?>">Delete</button>
                                </td>
                            </tr>
                            <?php
                            $iter++;
                        }
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    // $('#addCase').on('click', function () {
    //     var name = $('#caseName').val();
    //     var page = $('#casePage').val();
    //     var date = $('#caseDate').val();
    //     var proj = $('#caseProject').val();
    //     var content = $('#caseContent').val();
    //     var preview = $('#casePreviewUrl').val();
    //
    //     $.post(
    //         '',
    //         {
    //             'addCase':'',
    //             'name': name,
    //             'page': page,
    //             'date': date,
    //             'project' : proj,
    //             'content': content,
    //             'preview': preview
    //         },
    //         function () {
    //             alert('Кейс добавлен');
    //             $('.add-case').trigger('reset');
    //         }
    //     )
    // });

    $('.delCase').on('click', function () {
        var id = $(this).attr("id");
        id = id.split('-');
        id = id[1];

        $.post(
            '',
            {
                'delete': id
            },
            function () {
                $('#cases-table').load('/admin/cases #cases-table > *');
                alert('Проект удален');
            }
        )
    })
</script>
</html>
