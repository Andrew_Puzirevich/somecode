<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "xhtml11.dtd">
<html>
<head>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1251" />
    <TITLE>Добы день!</TITLE>
    <link rel="stylesheet" href="style.css">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <script src="/moment-with-locales.js"></script>
    <script src="/bootstrap-datetimepicker.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
</head>
<body>
<?php
session_start();
if ($_POST['back'] != '') {
    $_SESSION['back_page'] = $_POST['back'];
}
?>
<div class="back"><a href="<?php echo $_SESSION['back_page']; ?>"><button class="btn btn-danger">ВЕРНУТЬСЯ / ОТМЕНА</button></a></div>
<?php

require_once 'newFile.php';
$procredure = new Procedure();
//var_dump($_POST);
if (isset($_POST['editProject']))
{

//    if ($_POST['userfileEdit'] != ''){
//        $uploaddir = $_SERVER['DOCUMENT_ROOT'].'uploads'.DIRECTORY_SEPARATOR;
//        $uploadfile = $uploaddir . basename($_FILES['userfileEdit']['name']);
//
//        if (move_uploaded_file($_FILES['userfileEdit']['tmp_name'], $uploadfile)) {
//            $outFileLogo = DIRECTORY_SEPARATOR .'uploads'.DIRECTORY_SEPARATOR . basename($_FILES['userfileEdit']['name']);
//
//            echo '<script>
////$("#img-loaded").load("/admin/edit #img-loaded > *");
//$("#projectLogo").load("/admin/edit #projectLogoEdit > *");
//</script>';
//
//        } else {
//
//            echo '<script>alert("Возможная атака с помощью файловой загрузки!")</script>';
//        }
//    }
if (isset($_POST['editFormProject'])){
    $projectName = $_POST['projectNameEdit'];
    $projectDate = $_POST['projectDateCreateEdit'];
    $projectShortAbout = $_POST['projectShortAboutEdit'];
    $projectFullAbout = $_POST['projectFullAboutEdit'];
    $siteUrl = $_POST['projectSiteEdit'];
    $address = $_POST['projectAddressEdit'];
    $projectPhone = $_POST['projectPhoneEdit'];
    $id = $_POST['idProj'];
    $procredure->updateProject($projectName, $projectDate, $projectShortAbout, $projectFullAbout, $siteUrl, $address, $projectPhone, $id);
}
    $projToEdit = $procredure->getProjectByID($_POST['idProj']);
    while($row = $projToEdit->fetch(PDO::FETCH_ASSOC)){
        if ($_POST['editProject'] == 're'){
            echo '<div class="alert alert-success" style="text-align: center" role="alert">
  Компания обновлена, изменения сохранены!
</div>';
        }
        ?>
        <div class="content">
        <h3>Редактирование компании <?php echo $row['project_name']?></h3>

        <div class="editForm">

            <div id="img-loaded">
                <label for="projectLogo">Логотип проекта (в оригинале)</label>
                <div style="width: 100px;">
                    <?php echo $row['project_logo']; ?>
                </div>
            </div>

<!--            <div class="form-group ">-->
<!---->
<!--                <label for="projectLogoNew">Загрузить новый логотип</label>-->
<!--                <div class="custom-file">-->
<!--                    <form enctype="multipart/form-data" method="POST" action="">-->
<!--                        <p>Выберите файл: <input name="userfileEdit" type="file">-->
<!--                            <input type="hidden" name="idProj" value="--><?php //echo $row['id']?><!--">-->
<!--                            <input type="hidden" name="editProject">-->
<!--                        <button type="submit" name="submit">Загрузить</button></p>-->
<!--                    </form>-->
<!--                </div>-->
<!--            </div>-->
            <form method="post" action="">
                <?php
                if ($outFileLogo != ''){
                    $svg = file_get_contents('https://bohdan.red'.$outFileLogo);
                }

                ?>

<!--                <input type="hidden" id="projectLogoEdit" name="projectLogoEdit" value='--><?php //echo $svg; ?><!--'>-->
<!--                <div class="form-group">-->
<!--                    <label for="logoColor">Цвет лого</label>-->
<!--                    <input type="text" id="logoColor" name="logoColor" value="--><?php //echo $row['logo_color']?><!--">-->
<!--                    <div style="background-color: --><?php// echo $row['logo_color']?><!--                ; width: 30px; height: 30px;"></div>-->
<!--                </div>-->
                <div class="form-group">
                    <label for="projectName">Название</label>
                    <input type="text" id="projectName" name="projectNameEdit" value="<?php echo $row['project_name']?>">
                </div>
                <div class="form-group">
                    <label for="projectDateCreate">Дата создания</label>
                    <input type="date" id="projectDateCreate" name="projectDateCreateEdit" value="<?php echo date('Y-m-d', strtotime($row['create_date'])); ?>">
                </div>
                <div class="form-group">
                    <label for="projectShortAbout">Короткое описание</label>
                    <textarea cols="20" rows="3" id="projectShortAbout" name="projectShortAboutEdit"><?php echo $row['short_about_pr']?></textarea>
                </div>
                <div class="form-group">
                    <label for="projectFullAbout">Описание</label>
                    <textarea cols="20" rows="5" id="projectFullAbout" name="projectFullAboutEdit"><?php echo $row['about_pr']?></textarea>
                </div>
                <div class="form-group">
                    <label for="projectSite">Сайт</label>
                    <input type="text" id="projectSite" name="projectSiteEdit" value="<?php echo $row['site_url']?>">
                </div>
                <div class="form-group">
                    <label for="">Социальные сети проекта</label>
                    <input type="text" id="" name="">
                </div>
                <div class="form-group">
                    <label for="projectAddress">Адрес проекта</label>
                    <input type="text" id="projectAddress" name="projectAddressEdit" value="<?php echo $row['adress_pr']?>">
                </div>
                <div class="form-group">
                    <label for="projectPhone">Телефон проекта</label>
                    <input type="text" id="projectPhone" name="projectPhoneEdit" value="<?php echo $row['phone_pr']?>">
                </div>
                <div class="form-group">
                    <label for="">Новости проекта</label>
                    <input type="text" id="" name="">
                </div>
                <div class="form-group">
                    <label for="">Кейсы проекта</label>
                    <input type="text" id="" name="">
                </div>
                <div class="form-group">
                    <label for="">Люди в проекте</label>
                    <input type="text" id="" name="">
                </div>
                <div class="form-group">
                    <label for="">Услуги проекта</label>
                    <input type="text" id="" name="">
                </div>
                <div class="form-group">
                    <label for="">Клиенты проекта</label>
                    <input type="text" id="" name="">
                </div>
                <div class="form-group">
                    <label for="">Ачивки проекта</label>
                    <input type="text" id="" name="">
                </div>
                <div class="form-group">
                    <input type="hidden" name="idProj" value="<?php echo $row['id']; ?>">
                    <input type="hidden" name="editProject" value="re">
                    <button class="btn btn-success" id="editFormProject" name="editFormProject">Обновить</button>
                </div>
            </form>

        </div>
        </div>
        <?php
    }
}

if (isset($_POST['editCase'])){
    $caseToEdit = $procredure->getCaseById($_POST['idCase']);
    while ($row = $caseToEdit->fetch(PDO::FETCH_ASSOC)){
        var_dump($row);
        ?>
        <div class="content">
            <h3>Редактирование кейса <?php echo $row['case_name'] ?></h3>

            <div class="editForm">
                <form action="" method="post">
                    <div class="form-group">
                        <label for="">Превью</label>
                        <img src="..<?php echo $row['project_prewiev']?>" alt="<?php echo $row['case_name'] ?>" width="200">
                    </div>
                      <div class="form-group">
                        <label for="caseName">Название кейса</label>
                        <input type="text" id="caseName" name="caseName" value="<?php echo $row['case_name'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="casePage" id="">Номер кейса для URL</label>
                        <input type="text" name="casePage" id="casePage" value="<?php echo $row['case_page']?>">
                    </div>
                    <div class="form-group">
                        <label for="caseDate" id="">Дата публикации</label>
                        <input type="date" name="caseDate" id="caseDate"  value="<?php echo date('Y-m-d', strtotime($row['date_publish'])); ?>">
                    </div>
                    <div class="form-group">
                        <label for="" id="">Услуга к которой относится</label>
                        <input type="text" name="caseService" id="">
                    </div>
                    <div class="form-group">
                        <label for="" id="">Клиент для которого разрабатывался</label>
                        <input type="text" name="caseClient" id="" value="<?php echo $row['case_for_client']?>">
                    </div>
                    <div class="form-group">
                        <label for="caseProject" id="">Компания (Проект) которым разрабатывался</label>
                        <select name="caseProject" id="caseProject">
                            <?php
                            $companies = $procredure->getCompany();
                            while ($rowC = $companies->fetch(PDO::FETCH_ASSOC)){
                                ?>
                                <option value="<?php echo $rowC['id']?>" <?php if ($rowC['id'] == $row['case_project']) {echo 'selected';}?>><?php echo $rowC['project_name']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" id="">Ачивки, которые получал</label>
                        <input type="text" name="" id="">
                    </div>
                    <div class="form-group">
                        <label for="" id="">Люди, которые работали над проектом</label>
                        <input type="text" name="" id="">
                    </div>
                    <div class="form-group">
                        <label for="" id="">Отрасль к которой относится</label>
                        <input type="text" name="" id="">
                    </div>

                    <div class="form-group">
                        <div style="display: flex;">
                            <div style="width: 200px; height: 20px; color: #000; background-color: white">255,255,255 - белый</div>
                            <div style="background-color: black; color: white; width: 200px; height: 20px">0,0,0 - черный</div>
                        </div>
                        <label for="">Цвет фона кейса в rgb()</label>
                        <input placeholder="000,000,000" type="text" value="<?php echo $row['color_fon']?>" name="fon_color">

                        <label for="">Картинка фона кейса (первый экран)</label>
                        <input type="file" name="first_fon">
                    </div>
                    <div class="form-group">
                        <label for="">Цвет футера, хедера и текста в rgb()</label>
                        <input placeholder="000,000,000" type="text" value="<?php echo $row['color_head_foo']?>" name="foo_color" >
                    </div>
                    <h5 style="font-weight: 700">Редактор страницы</h5>
                    <div class="form-group">

                        <div id="summernote">
                            <?php
                            echo htmlspecialchars_decode(file_get_contents('../admin/cases-data/'.$row["case_page"].'.txt'));
                            ?>
                        </div>
                        <script>
                            $(document).ready(function() {
                                $('#summernote').summernote();
                            });

                            $('#summernote').summernote({
                                callbacks: {
                                    onChange: function(contents, $editable) {
                                        console.log('onChange:', contents, $editable);
                                        $('#page_text').val(contents);
                                    }
                                }
                            });

                        </script>
                    </div>
                    <input type="hidden" id="page_text" name="page_text" value="">
                    <h5>SEO</h5>
                    <div class="form-group">
                        <label for="" >Title кейса</label>
                        <input type="text" name="seo_title" value="<?php echo $row['seo_title']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Description кейса</label>
                        <input type="text" name="seo_description" value="<?php echo $row['seo_descr']?>">
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="idCase" value="<?php echo $row['id']; ?>">
                        <input type="hidden" name="editCase" value="re">
                        <button class="btn btn-success" name="editFormCase">Обновить</button>
                    </div>

                </form>
            </div>
        </div>
        <?php

    }
}
?>

</body>
</html>
