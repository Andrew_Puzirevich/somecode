<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "xhtml11.dtd">
<html>
<head>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1251" />
	<TITLE>Добы день!</TITLE>
	<link rel="stylesheet" href="style.css">

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<?php

require_once 'newFile.php';
$procredure = new Procedure();
$outFileLogo = '';
$projToEdit = '';

if (isset($_POST['editFormProject'])){
    $procredure->updateProject($_POST["logoColor"], $_POST["projectNameEdit"], $_POST["projectDateCreateEdit"], $_POST["projectShortAboutEdit"], $_POST["projectFullAboutEdit"], $_POST["projectSiteEdit"], $_POST["projectAddressEdit"], $_POST["projectPhoneEdit"], $_POST["idProject"]);
    echo '<script>alert("Проект обновлен")</script>';
}

if (isset($_POST['addProject'])){
    $procredure->setProject($_POST['logo'],$_POST['name'],$_POST['date'],$_POST['shAbout'],$_POST['flAbout'],$_POST['site'],$_POST['adr'],$_POST['phone'], $_POST['logoColor']);
}

if (isset($_POST['delete'])){
    $idDel = $_POST['delete'];
    $procredure->delProject($idDel);
}


if (isset($_FILES['userfile'])){
    $uploaddir = $_SERVER['DOCUMENT_ROOT'].'uploads'.DIRECTORY_SEPARATOR;
    $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
        $outFileLogo = DIRECTORY_SEPARATOR .'uploads'.DIRECTORY_SEPARATOR . basename($_FILES['userfile']['name']);

        echo '<script>
$("#img-loaded").load("/admin/index #img-loaded > *");
$("#projectLogo").load("/admin/index #projectLogo > *");
</script>';

    } else {

        echo '<script>alert("Возможная атака с помощью файловой загрузки!")</script>';
    }


}


?>

<body>
<div>
	<div class="row col-md-12">
		<div class="col-2">
            <?php require_once 'menu.html';?>
		</div>
        <div class="col-md-10 row block-block" id="projects-block">
            <div class="tab-pane-inside" id="projForms">
                <h3>Добавить проект</h3>
                <div class="addForm">
                    <div class="form-group ">
                        <label for="projectLogo">Логотип проекта</label>

                        <div class="custom-file">
                            <form name="uploader" enctype="multipart/form-data" method="POST">
                                Отправить этот файл: <input name="userfile" type="file" />
                                <button type="submit" name="submit">Загрузить</button>
                            </form>

                            <?php
                            if ($outFileLogo != ''){
                                $svg = file_get_contents('https://bohdan.red'.$outFileLogo);
                            }

                            ?>

                            <input type="hidden" id="projectLogo" value='<?php echo $svg; ?>'>
                        </div>
                    </div>
                    <div id="img-loaded">
                        <img src="<?php echo $outFileLogo; ?>" alt="" width="100">
                    </div>
                    <!--                            <div class="form-group">-->
                    <!--                                <label for="logoColor">Укажите цвет лого</label>-->
                    <!--                                <input type="text" id="logoColor" name="logoColor">-->
                    <!--                            </div>-->
                    <div class="form-group">
                        <label for="projectName">Название</label>
                        <input type="text" id="projectName" name="projectName">
                    </div>
                    <div class="form-group">
                        <label for="projectDateCreate">Дата создания</label>
                        <input type="date" id="projectDateCreate" name="projectDateCreate">
                    </div>
                    <div class="form-group">
                        <label for="projectShortAbout">Короткое описание</label>
                        <textarea cols="20" rows="3" id="projectShortAbout" name="projectShortAbout"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="projectFullAbout">Описание</label>
                        <textarea cols="20" rows="5" id="projectFullAbout" name="projectFullAbout"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="projectSite">Сайт</label>
                        <input type="text" id="projectSite" name="projectSite">
                    </div>
                    <div class="form-group">
                        <label for="">Социальные сети проекта</label>
                        <input type="text" id="" name="">
                    </div>
                    <div class="form-group">
                        <label for="projectAddress">Адрес проекта</label>
                        <input type="text" id="projectAddress" name="projectAddress">
                    </div>
                    <div class="form-group">
                        <label for="projectPhone">Телефон проекта</label>
                        <input type="text" id="projectPhone" name="projectPhone">
                    </div>
                    <div class="form-group">
                        <label for="">Новости проекта</label>
                        <input type="text" id="" name="">
                    </div>
                    <div class="form-group">
                        <label for="">Кейсы проекта</label>
                        <input type="text" id="" name="">
                    </div>
                    <div class="form-group">
                        <label for="">Люди в проекте</label>
                        <input type="text" id="" name="">
                    </div>
                    <div class="form-group">
                        <label for="">Услуги проекта</label>
                        <input type="text" id="" name="">
                    </div>
                    <div class="form-group">
                        <label for="">Клиенты проекта</label>
                        <input type="text" id="" name="">
                    </div>
                    <div class="form-group">
                        <label for="">Ачивки проекта</label>
                        <input type="text" id="" name="">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" id="addProject">Добавить</button>
                    </div>
                </div>

            </div>

            <div class="tab-pane-inside" id="projectTable">
                <h3>Список проектов</h3>
                <table class="table table-striped">

                    <tbody>

                    <?php
                    $projects = $procredure->getProjects();

                    if ($projects):
                        $iter = 1;

                        foreach ($projects as $project):
                            ?>
<!--                            <style>-->
<!--                                .project---><?php//// echo $project['id'];?><!-- svg path, .project---><?php ////echo $project['id'];?><!-- svg polygon{-->
<!--                                    /*fill: */--><?php //////echo $project['logo_color'];?><!--/* !important;*/-->
<!--                                /*}*/-->
<!--                            </style>-->
                            <tr>
                                <th scope="row"><?php echo $iter; ?></th>
                                <td class="project-<?php echo $project['id']; ?> img-svg-proj"><?php echo $project['project_logo']; ?></td>
                                <td><?php echo $project['project_name']; ?></td>
                                <td>
                                    <form action="https://www.bohdan.red/admin/edit" method="post">
                                        <input type="hidden" name="back" value="<?php echo $_SERVER['REQUEST_URI'];?>">

                                        <input type="hidden" name="idProj" value="<?php echo $project['id']?>">
                                        <button class="btn btn-info full-btn" name="editProject">Edit</button>

                                    </form>
                                    <button class="btn btn-danger delProject full-btn" id="delProject-<?php echo $project['id']?>">Delete</button>
                                </td>
                            </tr>
                            <?php
                            $iter++;
                        endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

	</div>
</div>
<script>
    "use strict";
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });


    $("form[name='uploader']").submit(function(e) {
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: '',
            type: "POST",
            data: formData,
        });
        e.preventDefault();
    });

    $('.delProject').on('click', function () {
        var id = $(this).attr("id");
        id = id.split('-');
        id = id[1];

        $.post(
            '',
            {
                'delete': id
            },
            function () {
                $('#projectTable').load('/admin/index #projectTable > *');
                alert('Проект удален');
            }
        )
    });

    $('#addProject').on('click', function () {
        var logo = $('#projectLogo').val();
        var name = $('#projectName').val();
        var date = $('#projectDateCreate').val();
        var shAbout = $('#projectShortAbout').val();
        var flAbout = $('#projectFullAbout').val();
        var site = $('#projectSite').val();
        var adr = $('#projectAddress').val();
        var phone = $('#projectPhone').val();
        var color = $('#logoColor').val();

        if (name !== ''){
            $.post(
                '',
                {
                    'addProject': "",
                    'logo':logo,
                    'name':name,
                    'date':date,
                    'shAbout': shAbout,
                    'flAbout':flAbout,
                    'site':site,
                    'adr':adr,
                    'phone': phone,
                    'logoColor':color
                },
                function (res, status) {
                    $('#projectTable').load('/admin/index #projectTable > *');
                    $('.addForm').load('/admin/index .addForm > *');
                    alert('Проект добавлен');
                }
            )
        } else alert('Имя не может быть пустым');

    });


</script>
</body>
</html>