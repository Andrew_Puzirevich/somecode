<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "xhtml11.dtd">
<html>
<head>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1251" />
    <TITLE>Добы день!</TITLE>
    <link rel="stylesheet" href="style.css">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
</head>

<body>
<?php
require_once 'newFile.php';
$procredure = new Procedure();

if (isset($_POST['addNews'])){
    $procredure->setNews(htmlspecialchars($_POST['news_text']), $_POST['news_date']);
    echo '<div class="alert alert-success" role="alert">
  Новость добавлена!
</div>';
}
?>
<div>
    <div class="row col-md-12">
        <div class="col-2">
            <?php require_once 'menu.html';?>
        </div>
        <div class="col-md-10 row block-block" id="cases-block">
            <div class="tab-pane-inside">
                <h3>Добавить новость</h3>
                <form action="" method="post" class="addNews">
                    <div class="form-group">
                        <label for="">Новость</label>
                        <div id="summernote">

                        </div>
                        <script>
                            $(document).ready(function() {
                                $('#summernote').summernote();
                            });

                            $('#summernote').summernote({
                                callbacks: {
                                    onChange: function(contents, $editable) {
                                        console.log('onChange:', contents, $editable);
                                        $('#news_text').val(contents);
                                    }
                                }
                            });
                        </script>
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Дата</label>
                        <input type="date" name="news_date">
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="news_text" id="news_text">
                        <button class="btn btn-success" name="addNews">Добавить</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane-inside" id="cases-table">
                <h3>Список новостей</h3>
                <table class="table table-striped">
                    <tbody>
                    <?php
                    $news = $procredure->getNews();
                    if (!empty($news)):
                        $iter = 1;
                        foreach ($news as $new) {
                            ?>
                            <tr>
                                <th scope="row"><?php echo $iter; ?></th>

                                <td><?php echo $new['news']?></td>
                                <td>
                                    <form action="https://www.bohdan.red/admin/edit" method="post">
                                        <input type="hidden" name="back" value="<?php echo $_SERVER['REQUEST_URI'];?>">

                                        <input type="hidden" name="idNews" value="<?php echo $new['id']?>">
                                        <button class="btn btn-info full-btn" name="editNews">Edit</button>

                                    </form>
                                    <button class="btn btn-danger delNews full-btn" id="delNews-<?php echo $new['id']?>">Delete</button>
                                </td>
                            </tr>
                            <?php
                            $iter++;
                        }
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>